# Singleton SD - Sitespeed.io

Creates a docker image of sitespeed.io setting the entry point to default in order to use it inside jenkins.

## Documentation

[Github Repository](https://github.com/sitespeedio/sitespeed.io)

[Dockerhub](https://hub.docker.com/r/sitespeedio/sitespeed.io/)

----------------------

© [Singleton](http://singletonsd.com), Italy, 2019.
